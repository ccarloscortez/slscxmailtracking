from database import db
class ClickTracking(db.Model):
	__tablename__ = 'cx_click_tracking'
	id_click = db.Column(db.Integer, primary_key=True)
	id_cliente = db.Column(db.String(80), nullable=False)
	click_location = db.Column(db.String(80), nullable=False)
	platform = db.Column(db.String(80), nullable=False)
	original_link = db.Column(db.String(120), nullable=False)
	geo_country = db.Column(db.String(50), nullable=False)
	geo_region = db.Column(db.String(80), nullable=False)
	geo_city = db.Column(db.String(50), nullable=False)
	geo_coords = db.Column(db.String(80), nullable=False)
	message_id = db.Column(db.String(100), nullable=False)
	received_at = db.Column(db.DateTime(), nullable=False)
	tag = db.Column(db.String(80), nullable=False)
	recipient = db.Column(db.String(120), nullable=False)

class OpenTracking(db.Model):
	__tablename__ = 'cx_open_tracking'
	id_open = db.Column(db.Integer, primary_key=True)
	id_cliente = db.Column(db.String(80), nullable=False)
	platform = db.Column(db.String(80), nullable=False)
	geo_country = db.Column(db.String(50), nullable=False)
	geo_region = db.Column(db.String(80), nullable=False)
	geo_city = db.Column(db.String(50), nullable=False)
	geo_coords = db.Column(db.String(80), nullable=False)
	message_id = db.Column(db.String(100), nullable=False)
	received_at = db.Column(db.DateTime(), nullable=False)
	tag = db.Column(db.String(80), nullable=False)
	recipient = db.Column(db.String(120), nullable=False)

class DeliveryTracking(db.Model):
	__tablename__ = 'cx_delivery_tracking'
	id_delivery = db.Column(db.Integer, primary_key=True)
	id_cliente = db.Column(db.String(80), nullable=False)
	message_id = db.Column(db.String(100), nullable=False)
	delivered_at = db.Column(db.DateTime(), nullable=False)
	tag = db.Column(db.String(80), nullable=False)
	recipient = db.Column(db.String(120), nullable=False)

class BounceTracking(db.Model):
	__tablename__ = 'cx_bounce_tracking'
	id_bounce = db.Column(db.Integer, primary_key=True)
	id_cliente = db.Column(db.String(80), nullable=False)
	message_id = db.Column(db.String(100), nullable=False)
	bounce_type = db.Column(db.String(100), nullable=False)
	bounced_at = db.Column(db.DateTime(), nullable=False)
	tag = db.Column(db.String(80), nullable=False)
	recipient = db.Column(db.String(120), nullable=False)
	dump_available = db.Column(db.Boolean)
	inactive = db.Column(db.Boolean)
	can_activate = db.Column(db.Boolean)

class InboundTracking(db.Model):
	__tablename__ = 'cx_inbound_tracking'
	id_inbound = db.Column(db.Integer, primary_key=True)
	id_cliente = db.Column(db.String(80), nullable=False)
	message_id = db.Column(db.String(100), nullable=False)
	from_name = db.Column(db.String(100), nullable=False)
	from_email = db.Column(db.String(100), nullable=False)
	recipient = db.Column(db.String(100), nullable=False)
	reply_to = db.Column(db.String(100), nullable=False)
	subject = db.Column(db.String(100), nullable=False)
	text_body = db.Column(db.String(1000), nullable=True)
	html_body = db.Column(db.String(1000), nullable=True)
	tag = db.Column(db.String(100), nullable=False)
	date = db.Column(db.DateTime(), nullable=False)
	