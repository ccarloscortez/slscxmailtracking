import json
import os
from app import app
from database import configure_db
from models import ClickTracking, OpenTracking, DeliveryTracking, BounceTracking, InboundTracking
from datetime import datetime
from sqlalchemy.ext.declarative import DeclarativeMeta
import http

def get_clicks_list(event, context):
	initialize(event)
	campaign_id = event['tag']
	rs = ClickTracking.query.filter_by(tag=campaign_id).all()
	return response(rs)

def get_open_list(event, context):
	initialize(event)
	campaign_id = event['tag']
	rs = OpenTracking.query.filter_by(tag=campaign_id).all()
	return response(rs)

def get_delivery_list(event, context):
	initialize(event)
	campaign_id = event['tag']
	rs = DeliveryTracking.query.filter_by(tag=campaign_id).all()
	return response(rs)

def get_bounce_list(event, context):
	initialize(event)
	campaign_id = event['tag']
	rs = BounceTracking.query.filter_by(tag=campaign_id).all()
	return response(rs)

def get_inbound_list(event, context):
	initialize(event)
	campaign_id = event['tag']
	rs = InboundTracking.query.filter_by(tag=campaign_id).all()
	return response(rs)

def initialize(event):
	if "connection_string" in event:
		configure_db(app, event["connection_string"])
	if "tag" not in event:
		raise Exception("No tag inside input json")
	app.app_context().push()

def response(resultList):
	result = json.dumps(resultList, default=alchemy_encoder)
	responseData = json.loads(result)
	return {
		'status': http.HTTPStatus.OK,
		'response': responseData
	}

def alchemy_encoder(obj):
    if isinstance(obj.__class__, DeclarativeMeta):
        fields = {}
        for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
            data = obj.__getattribute__(field)
            try:
            	if isinstance(data.__class__, DeclarativeMeta):
            		fields[field] = alchemy_encoder(data)
            	elif isinstance(data, datetime):
            		fields[field] = data.isoformat()
            	else:
	                json.dumps(data)
	                fields[field] = data
            except TypeError:
                fields[field] = None
        return fields
    return json.JSONEncoder.default(self, obj)