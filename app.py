from flask import Flask, render_template, request
from database import db, configure_db
from models import ClickTracking, OpenTracking, DeliveryTracking, BounceTracking, InboundTracking
import os
import dateutil.parser
import boto3
from random import randint
import json
app = Flask(__name__)
if "connection_string" in os.environ:
	configure_db(app, os.environ.get("connection_string"))
def initialize_sqs():
	if "SQS_REGION" not in os.environ:
		raise Exception("SQS_REGION not provided")
	if "SQS_ACCESS_KEY" in os.environ and "SQS_SECRET_KEY" in os.environ:
		return boto3.client('sqs', aws_access_key_id=os.environ["SQS_ACCESS_KEY"], aws_secret_access_key=os.environ["SQS_SECRET_KEY"], region_name=os.environ["SQS_REGION"])
	else:
		return boto3.client('sqs', region_name=os.environ["SQS_REGION"])

def send_sqs(api_id, request_json, id_cliente):
	if "SQS_QUEUE_URL" not in os.environ:
		print("SQS_QUEUE_URL evnironment variable missing..")
		return
	sqs = initialize_sqs()
	event_log = {}
	event_log["input"] = request_json
	event_log["input"]["id_api"] = api_id
	event_log["input"]["id_cliente"] = id_cliente
	queue_url = os.environ["SQS_QUEUE_URL"]
	sqs.send_message(
            QueueUrl=queue_url,
            MessageDeduplicationId=event_log["input"]['id_api']+"_"+str(randint(0, 99999999999999999999)),
            MessageBody=json.dumps(event_log),
            MessageGroupId="cxtest"
        )

def get_client_id(data):
	if request.args.get("id_cliente", ""):
		return request.args.get("id_cliente")
	elif "Metadata" in data and "id_cliente" in data["Metadata"]:
		return data["Metadata"]["id_cliente"]
	elif os.environ.get("id_cliente"):
		return os.environ.get("id_cliente")
	else:
		return data["Tag"]

@app.route("/", methods=["GET", "POST"])
@app.route("/click", methods=["POST"])
def handle_clicktracking():
	if request.json is None:
		return ("no json provided", 400)
	else:
		data = request.json
		entry = ClickTracking()
		entry.id_cliente = get_client_id(data)
		entry.click_location = data["ClickLocation"]
		entry.platform = data["Platform"]
		entry.original_link = data["OriginalLink"]
		entry.geo_country = data["Geo"]["Country"]
		entry.geo_region = data["Geo"]["Region"]
		entry.geo_city = data["Geo"]["City"]
		entry.geo_coords = data["Geo"]["Coords"]
		entry.message_id = data["MessageID"]
		entry.received_at = dateutil.parser.parse(data["ReceivedAt"])
		entry.tag = data["Tag"]
		entry.recipient = data["Recipient"]
		db.create_all()
		db.session.add(entry)
		db.session.commit()
		send_sqs("cx_click_tracking", request.json, entry.id_cliente)
		return "Success"

@app.route("/open", methods=["POST"])
def handle_opentracking():
	if request.json is None:
		return ("no json provided", 400)
	else:
		data = request.json
		entry = OpenTracking()
		entry.id_cliente = get_client_id(data)
		entry.platform = data["Platform"]
		entry.geo_country = data["Geo"]["Country"]
		entry.geo_region = data["Geo"]["Region"]
		entry.geo_city = data["Geo"]["City"]
		entry.geo_coords = data["Geo"]["Coords"]
		entry.message_id = data["MessageID"]
		entry.received_at = dateutil.parser.parse(data["ReceivedAt"])
		entry.tag = data["Tag"]
		entry.recipient = data["Recipient"]
		db.create_all()
		db.session.add(entry)
		db.session.commit()
		send_sqs("cx_open_tracking", request.json, entry.id_cliente)
		return "Success"

@app.route("/delivery", methods=["POST"])
def handle_deliverytracking():
	if request.json is None:
		return ("no json provided", 400)
	else:
		data = request.json
		entry = DeliveryTracking()
		entry.id_cliente = get_client_id(data)
		entry.message_id = data["MessageID"]
		entry.delivered_at = dateutil.parser.parse(data["DeliveredAt"])
		entry.tag = data["Tag"]
		entry.recipient = data["Recipient"]
		db.create_all()
		db.session.add(entry)
		db.session.commit()
		send_sqs("cx_delivery_tracking", request.json, entry.id_cliente)
		return "Success"

@app.route("/bounce", methods=["POST"])
def handle_bouncetracking():
	if request.json is None:
		return ("no json provided", 400)
	else:
		data = request.json
		entry = BounceTracking()
		entry.id_cliente = get_client_id(data)
		entry.message_id = data["MessageID"]
		entry.bounce_type = data["TypeCode"]
		entry.bounced_at = dateutil.parser.parse(data["BouncedAt"])
		entry.tag = data["Tag"]
		entry.recipient = data["Email"]
		entry.dump_available = data["DumpAvailable"]
		entry.inactive = data["Inactive"]
		entry.can_activate = data["CanActivate"]
		db.create_all()
		db.session.add(entry)
		db.session.commit()
		send_sqs("cx_bounce_tracking", request.json, entry.id_cliente)
		return "Success"

@app.route("/inbound", methods=["POST"])
def handle_inboundtracking():
	if request.json is None:
		return ("no json provided", 400)
	else:
		data = request.json
		entry = InboundTracking()
		entry.id_cliente = get_client_id(data)
		entry.message_id = data["MessageID"]
		entry.date = dateutil.parser.parse(data["Date"])
		entry.tag = data["Tag"]
		entry.recipient = data["OriginalRecipient"]
		entry.from_name = data["FromName"]
		entry.from_email = data["From"]
		entry.reply_to = data["ReplyTo"]
		entry.subject = data["Subject"]
		if "TextBody" in data:
			entry.text_body = data["TextBody"]
		if "HtmlBody" in data:
			entry.html_body = data["HtmlBody"]
		db.create_all()
		db.session.add(entry)
		db.session.commit()
		send_sqs("cx_inbound_tracking", request.json, entry.id_cliente)
		return "Success"


if __name__ == "__main__":
    app.run()
