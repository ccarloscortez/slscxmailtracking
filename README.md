## Usage

```
$ npm install
$ serverless deploy
```

Once the deploy is complete, run `sls info` to get the endpoint:

```
$ sls info
Service Information
<snip>
endpoints:
  ANY - https://abc6defghi.execute-api.us-east-1.amazonaws.com/dev <-- Endpoint
  ANY - https://abc6defghi.execute-api.us-east-1.amazonaws.com/dev/{proxy+}
```
Click Tracking 	-->  endpoint + `/click`  
Open Tracking 	-->  endpoint + `/open`  
Delivery Tracking --> endpoint + `/delivery`  
Bounce Tracking --> endpoint + `/bounce`  
Inbound Tracking --> endpoint + `/inbound`  

## `connection_string`

Pass as environment variable under provider or function config
```
provider:
  name: aws
  runtime: python3.6
  stage: dev
  region: us-east-1
  environment:
    connection_string: "dbname=artucx user=artucx password=artucx host=example.artucx.ai"
```

## `id_cliente`

1. Pass as environment variable
2. Pass as query parameter `?id_cliente=client_name`
3. Add as `Metadata` with the email

## SQS implementation
Pass as environmnet variable under provider or function config
```
environment:
    SQS_QUEUE_URL: "https://sqs.us-east-1.amazonaws.com/467013257237/artucx.fifo"
    SQS_REGION: 'us-east-1'
    SQS_ACCESS_KEY: access_key_here
    SQS_SECRET_KEY: secret_key_here
```
`SQS_QUEUE_URL` and `SQS_REGION` must be provided. Others are optional.

## Local development

To develop locally, create a virtual environment and install your dependencies:

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

Then, run your app:

```
sls wsgi serve
 * Running on http://localhost:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
```

Navigate to [localhost:5000](http://localhost:5000) to see your app running locally.
