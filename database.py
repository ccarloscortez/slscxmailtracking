from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
def configure_db(app, connection_string):
	connection_config = parse_connection_string(connection_string)
	if "driver" not in connection_config:
		connection_config["driver"] = "postgresql"
	app.config["SQLALCHEMY_DATABASE_URI"] = '{driver}://{user}:{password}@{host}/{dbname}'.format(**connection_config)
	app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
	db.init_app(app)

def parse_connection_string(connection_string):
	arr = list(map(lambda s: s.split("="), connection_string.strip().split(" ")))
	config = {}
	for item in arr:
		config[item[0]] = item[1]
	return config
